# PRoot Static Multi Architecture

_PRoot static binaries for multiple architectures._

## License

SPDX-License-Identifier: [AGPL-3.0](COPYING)

## Reference

- [proot-static-build](https://github.com/proot-me/proot-static-build)

## See Also

- [alpine-chroot-install](https://github.com/alpinelinux/alpine-chroot-install)

