#!/bin/sh
# PRoot static binaries for multiple architectures
# SPDX-License-Identifier: AGPL-3.0
set -eu

VERSION="0.0.1"

ALPINE_DEPENDENCIES="
  bash
  bsd-compat-headers
  clang
  clang-analyzer
  coreutils
  gcc
  git
  grep
  libarchive-dev
  libarchive-static
  linux-headers
  lzo
  make
  mcookie
  musl-dev
  python2-dev
  swig
  talloc-dev
  talloc-static
  uthash-dev
"

ALPINE_URL="https://dl-cdn.alpinelinux.org/alpine/v3.15/releases"

ALPINE_VERSION="3.15.0"

debug=0
verbose=0
quiet=0

usage() {
  cat <<EOF
Usage: proot-static-multiarch [option] ... [command]

Options:
  -h, --help            Show this message and quit
  -d, --debug           Output debugging messages
  -q, --quiet           Only output fatal error messages
  -v, --verbose         Be verbose (show external command output)
  --version             Print version and exit

Arguments:
  arch                  Architecture to build
  version               PRoot version to build
EOF
}

create_rootfs() {
  rootfs_dir=$1
  rootfs_url=$2
  rootfs_archive=$3
  debug=$4
  verbose=$5
  quiet=$6

  archive_url="${rootfs_url}/${rootfs_archive}"

  if [ "${verbose}" = 1 ]; then
    echo "archive_url is ${archive_url}"
  fi

  mkdir -p "${rootfs_dir}"

  cd "${rootfs_dir}"

  if [ "${debug}" = 1 ]; then
    echo "rootfs_dir is $(pwd)"
  fi

  if [ "${quiet}" = 1 ]; then
    curl -LO "${archive_url}" > /dev/null 2>&1
  else
    curl -LO "${archive_url}"
  fi

  tar -xf "./${rootfs_archive}"

  rm "./${rootfs_archive}"
}

clone_source() {
  rootfs_dir=$1
  verbose=$2
  quiet=$3

  source_dir="${rootfs_dir}/usr/src"

  mkdir -p "${source_dir}"

  source_dir="${source_dir}/proot"

  if [ "${verbose}" = 1 ]; then
    echo "source_dir is ${source_dir}"
  fi

  if [ "${quiet}" = 1 ]; then
    git clone https://github.com/proot-me/proot.git "${source_dir}/proot" > /dev/null 2>&1
  else
    git clone https://github.com/proot-me/proot.git "${source_dir}/proot"
  fi
}

proot_qemu_exec() {
  arch=$1
  rootfs=$2
  command=$3
  debug=$4
  quiet=$5

  if [ "${debug}" = 1 ]; then
    echo "proot_command is proot -q qemu-${arch} -S ${rootfs} ${command}"
  fi

  if [ "${quiet}" = 1 ]; then
    proot -q "qemu-${arch}" -S "${rootfs}" "${command}" > /dev/null 2>&1
  else
    proot -q "qemu-${arch}" -S "${rootfs}" "${command}"
  fi
}

main() {

  while [ ${#} -gt 0 ]
  do
    argument=${1}
    shift
    case "${argument}" in
      -h|--help)
        usage
        exit 0
        ;;
      -d|--debug)
        debug=1
        ;;
      -q|--quiet)
        if [ "${verbose}" = 1 ]; then
          echo "The --quiet and --verbose options are mutually exclusive"
          exit 1
        fi
        quiet=1
	;;
      -v|--verbose)
        if [ "${quiet}" = 1 ]; then
          echo "The --quiet and --verbose options are mutually exclusive"
          exit 1
        fi
        verbose=1
        ;;
      --version)
        echo "proot-static-multiarch version ${VERSION}"
        exit 0
        ;;
      -*)
        echo "You have specified an invalid option: ${argument}"
        exit 1
        ;;
      aarch64|arm)
        arch="${argument}"
        ;;
      *)
        proot_version="${argument}"
        ;;
    esac
  done

  if [ "${debug}" = 1 ]; then
    echo "debug is ${debug}"
    echo "verbose is ${verbose}"
  fi


  alpine_archive="alpine-minirootfs-${ALPINE_VERSION}-${arch}.tar.gz"

  alpine_rootfs="./alpine-${arch}"

  if [ "${verbose}" = 1 ]; then
    echo "arch is ${arch}"
    echo "proot_version is ${proot_version}"
    echo "alpine_archive is ${alpine_archive}"
    echo "alpine_rootfs is ${alpine_rootfs}"
  fi

  create_rootfs "${alpine_rootfs}" "${ALPINE_URL}/${arch}" "${alpine_archive}" "${debug}" "${verbose}" "${quiet}"

  proot_qemu_exec "${arch}" "${alpine_rootfs}" "/sbin/apk add ${ALPINE_DEPENDENCIES}" "${debug}" "${quiet}"

  clone_source "${alpine_rootfs}" "${verbose}" "${quiet}"

  # shellcheck disable=SC2016
  proot_qemu_exec "${arch}" "${alpine_rootfs}" 'LDFLAGS="${LDFLAGS} -static -ltalloc" make -C /usr/src/proot/src loader.elf build.h' "${debug}" "${quiet}"

  # shellcheck disable=SC2016
  proot_qemu_exec "${arch}" "${alpine_rootfs}" 'LDFLAGS="${LDFLAGS} -static -ltalloc" make -C /usr/src/proot/src proot' "${debug}" "${quiet}"

  cp "${alpine_rootfs}/usr/src/proot/src/proot" "./proot-${proot_version}-${arch}-static"
}

if [ "$(basename "${0}")" = "proot-static-multiarch" ]; then
  main "${@}"
fi

